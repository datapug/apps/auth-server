package auth

import (
	"os"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/jwtauth"
)

var TokenAuth *jwtauth.JWTAuth

func init() {
	TokenAuth = jwtauth.New("HS256", []byte(os.Getenv("TOKEN_KEY")), nil)
}

func GenerateToken(claims map[string]interface{}) string {
	_, token, _ := TokenAuth.Encode(jwt.MapClaims(claims))
	return token
}
