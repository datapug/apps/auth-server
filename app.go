package main

import (
	"danwa-api/utils"
	"log"
	"net/http"
	"time"

	apiPub "auth-server/api/public"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/jwtauth"
	"github.com/go-chi/render"
)

func main() {

	router := routes()

	walkFunc := func(
		method string,
		route string,
		handler http.Handler,
		middlewares ...func(http.Handler) http.Handler,
	) error {
		log.Printf("%s %s\n", method, route) // Walk and print out all routes
		return nil
	}

	if err := chi.Walk(router, walkFunc); err != nil {
		log.Panicf("Logging err: %s\n", err.Error()) // panic if error
	}

	log.Fatal(http.ListenAndServe(":12000", router))
}

func routes() *chi.Mux {
	router := chi.NewRouter()
	router.Use(
		render.SetContentType(render.ContentTypeJSON), // Set Content-Type headers to always JSON
		middleware.Logger,          // Log API requests
		middleware.DefaultCompress, // Compress results - gzipping/json
		middleware.RedirectSlashes, // Redirect slashes to no slash URL versions
		middleware.Recoverer,       // Recover from panics without crashing server
	)

	// Set a timeout value on the request context (ctx), that will signal
	// through ctx.Done() that the request has timed out and further
	// processing should be stopped.
	router.Use(middleware.Timeout(30 * time.Second))

	router.Route("/api", func(r chi.Router) {

		// PROTECTED ENDPOINTS
		r.Group(func(r chi.Router) {
			// Use JWT Authentication
			r.Use(
				// Seek, Verify, and Validate tokens
				jwtauth.Verifier(utils.TokenAuth),
				// Handle valid/invalid tokens
				jwtauth.Authenticator,
			)

		})

		// PUBLIC ENDPOINTS - No Authorization Needed
		r.Group(func(r chi.Router) {
			r.Mount("/", apiPub.HealthCheckRoutes())
			r.Mount("/v1/register", apiPub.RegisterRoutes())
			r.Mount("/login", apiPub.LoginRoutes())
		})

	})

	return router

}
