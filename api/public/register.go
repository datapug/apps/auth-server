package api

import (
	"auth-server/db"
	"encoding/json"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"go.mongodb.org/mongo-driver/bson"
	"golang.org/x/crypto/bcrypt"
)

// RegisterRoutes API
func RegisterRoutes() *chi.Mux {
	router := chi.NewRouter()

	// define routes
	router.Post("/register", register)

	return router
}

func register(w http.ResponseWriter, r *http.Request) {
	response := make(map[string]interface{})

	decoder := json.NewDecoder(r.Body)

	var a db.Account
	err := decoder.Decode(&a)

	if err != nil {
		log.Fatal(err)
	}

	filter := bson.M{"username": a.Username}
	var result db.Account
	db.FindAccount(filter).Decode(&result)
	if result.Username == a.Username {
		http.Error(w, "Username already taken!", 500)
		return
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(a.Password), bcrypt.MinCost)
	if err != nil {
		log.Fatal(err)
	}
	a.Password = string(hash)

	id, err := db.AddAccount(a)

	if err != nil {
		log.Fatal(err)
	} else {
		log.Printf("Added account: %s", id)
	}

	response["New Account"] = map[string]interface{}{
		"ID":       id,
		"Username": a.Username,
		"Email":    a.Email,
	}

	render.JSON(
		w,
		r,
		response,
	)
}
