package api

import (
	"auth-server/auth"
	"auth-server/db"
	"encoding/json"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"go.mongodb.org/mongo-driver/bson"
	"golang.org/x/crypto/bcrypt"
)

// LoginRoutes API
func LoginRoutes() *chi.Mux {
	router := chi.NewRouter()

	// define routes
	router.Post("/login", login)

	return router
}

func login(w http.ResponseWriter, r *http.Request) {
	response := make(map[string]string)

	decoder := json.NewDecoder(r.Body)

	var a db.Account
	err := decoder.Decode(&a)

	if err != nil {
		log.Fatal(err)
	}

	filter := bson.M{"username": a.Username}
	var result db.Account
	db.FindAccount(filter).Decode(&result)

	byteHash := []byte(result.Password)
	err = bcrypt.CompareHashAndPassword(byteHash, []byte(a.Password))
	if err != nil {
		log.Println(err)
		// return error
		http.Error(w, err.Error(), 500)
		return
	}

	response["Login"] = "Success!"
	claims := map[string]interface{}{
		"user":  a.Username,
		"roles": []string{"user"},
	}
	response["Token"] = auth.GenerateToken(claims)

	render.JSON(
		w,
		r,
		response,
	)
}
