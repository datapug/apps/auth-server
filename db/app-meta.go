package db

import (
	"context"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"golang.org/x/crypto/bcrypt"
)

type AppSecret struct {
	Name   string
	Secret string
}

func generateAppSecret() {

	log.Println("Checking for App Secret...")

	collection := DBConn.Collection("meta")
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	//var a appSecret
	var a AppSecret
	collection.FindOne(ctx, bson.M{"name": "App Secret"}).Decode(&a)

	if a.Secret == "" {
		log.Println("No App Secret found!")

		// Generate an application secret random hash based on epoch time
		hash, _ := bcrypt.GenerateFromPassword([]byte(time.Now().String()), bcrypt.MinCost)

		log.Println("Generating App Secret...")

		_, err := collection.InsertOne(ctx, AppSecret{Name: "App Secret", Secret: string(hash)})
		if err != nil {
			log.Fatal("Failed to insert generated secret into DB...")
		}
		log.Println("Done! Please log into the DB to retrieve the App Secret.")

	} else {
		log.Println("Found Existing App Secret!")
	}
}
