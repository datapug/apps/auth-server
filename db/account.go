package db

import (
	"context"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type Account struct {
	Username  string
	Password  string
	Roles     []string
	FirstName string
	LastName  string
	Email     string
}

// AddAccount - insert one record into DB
func AddAccount(a Account) (interface{}, error) {

	collection := DBConn.Collection("accounts")
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	res, err := collection.InsertOne(ctx, a)
	id := res.InsertedID

	return id, err
}

// Look up an account
func FindAccount(filter bson.M) *mongo.SingleResult {

	collection := DBConn.Collection("accounts")
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	res := collection.FindOne(ctx, filter)
	log.Println(res)
	return res
}
